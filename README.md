# SpartanCorner

Spartan Corner application connects NSU faculty, staff, and students to local vendors in the community. Customers (faculty, staff, students) have the ability to view, search, and rate vendor profiles. Vendors can view/edit their profile.